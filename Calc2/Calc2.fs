﻿// Learn more about F# at http://fsharp.org

open System

let mutable numbers : int array = Array.zeroCreate 10

let rec readlines () = seq {
  let line = Console.ReadLine()
  if line <> null then
      yield line
      yield! readlines ()
}

for line in readlines() do
  let tab = line.Split(' ') 
  let a = Int32.Parse tab.[1] 
  let b = Int32.Parse tab.[2] 
  match tab.[0] with
    | "+" -> printfn"%d" (numbers.[a] + numbers.[b])
    | "-" -> printfn"%d" (numbers.[a] - numbers.[b])
    | "*" -> printfn"%d" (numbers.[a] * numbers.[b])
    | "/" -> printfn"%d" (numbers.[a] / numbers.[b])
    | "%" -> printfn"%d" (numbers.[a] % numbers.[b])
    | _ -> numbers.[a] <- b  