﻿// Learn more about F# at http://fsharp.org

open System

let rec calc test count = 
    if test < count then
        let n = Console.ReadLine() |> Int32.Parse
        let f1 = int64(2*n+1)
        let f2 = int64(n+1)*int64(n)
        let f3 = f1*f2
        let kw = f3/int64(6)
        printfn "%i" kw
        calc (test+1) count

[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let tests = Int32.Parse(T)
    calc 0 tests
    0
