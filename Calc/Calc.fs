﻿// Learn more about F# at http://fsharp.org

open System

let add x y = x + y
let sub x y = x - y
let mul x y = x * y
let div x y = x / y
let MOD x y = x % y

let rec readlines () = seq {
  let line = Console.ReadLine()
  if line <> null then
      yield line
      yield! readlines ()
}


for line in readlines() do
  let tab = line.Split(' ')
  let a = Int32.Parse tab.[1] 
  let b = Int32.Parse tab.[2] 
  match tab.[0] with
    | "+" -> printfn"%d" (add a b)
    | "-" -> printfn"%d" (sub a b)
    | "*" -> printfn"%d" (mul a b)
    | "/" -> printfn"%d" (div a b)
    | "%" -> printfn"%d" (MOD a b)