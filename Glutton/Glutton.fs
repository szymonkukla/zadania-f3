﻿// Learn more about F# at http://fsharp.org

open System

let rec run currTest maxTest = 
    if currTest < maxTest then
        let line = Console.ReadLine() 
        let tab = line.Split(' ') 
        let ilu = Int32.Parse tab.[0] 
        let ile = Int32.Parse tab.[1] 
        let mutable eated = 0
        for i in 1..ilu  do  
            let list = Int32.Parse(Console.ReadLine())           
            eated <- eated + 86400 / list 
        eated |> ignore  
        let odp =  eated 
        let odp1 =  odp |> float
        let odp2 =  odp1/(float ile)
        let odp3 = ceil odp2 |> int
        printfn "%A"  odp3 
        run (currTest+1) maxTest

[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    run 0 numCases
    0
